from django.urls import path
from . import views

urlpatterns = [
    path('', views.main_view, name='main_page'),
    path('add/', views.create_book, name='book_add'),
    path('<int:pk>/edit/', views.edit_book, name='book_edit'),
    path('category/add/', views.create_category, name='category_add'),
    path('<int:pk>/edit/', views.edit_category, name='category_edit'),
    path('<int:pk>/edit/', views.edit_book, name='book_edit'),
]
