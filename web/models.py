from django.db import models


class Category(models.Model):
    name = models.CharField(max_length=25)

    class Meta:
        app_label = 'web'

    def __str__(self):
        return self.name


class Book(models.Model):
    title = models.CharField(max_length=50)
    author = models.CharField(max_length=250)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    date = models.DateField()

    def __str__(self):
        return self.title
